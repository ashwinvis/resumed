import { promises as fs } from 'fs'
import TOML from '@ltd/j-toml'
import path from 'path'

export const init = (filename: string) => {
  const resume = require('resume-schema/sample.resume.json')
  if (path.extname(filename) === '.toml') {
    return fs.writeFile(filename, TOML.stringify(resume, { newline: '\n' }))
  } else {
    return fs.writeFile(filename, JSON.stringify(resume, undefined, 2))
  }
}
