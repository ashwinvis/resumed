import { promises as fs } from 'fs'
import TOML from '@ltd/j-toml'
import path from 'path'

export const resumeParse = async (filename: string) => {
  const resume = await fs.readFile(filename, 'utf-8')
  if (path.extname(filename) === '.toml') {
    return TOML.parse(resume, { joiner: '\n' })
  } else {
    return JSON.parse(resume)
  }
}
