import schema from 'resume-schema'
import { promisify } from 'util'
import { resumeParse } from './resume-parse'

const schemaValidate = promisify(schema.validate)

export const validate = async (filename: string) => {
  return schemaValidate(await resumeParse(filename))
}
